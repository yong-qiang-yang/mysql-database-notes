 1.Sqlyog方式
  
  1)选中要备份的数据库,右键备份导出---->选择备份数据库
  2)指定文件位置,选择导出即可
 
 2.Sqlyog数据库恢复
   
   1)先删除数据库
     drop database db2;
   2)导入之前备份的SQL文件
 
 3.命令行备份
   
   进入到Mysql安装目录的bin目录下,打开DOS命令行
   1)语法格式
   mysqldump -u 用户名 -p 密码 数据库 > 文件路径
   2)执行备份,备份db2中的数据到H盘的db02.sql文件中
   mysqldump -uroot -p123456 db2 > E:/db02.sql
   
 4.命令行恢复
  
   1)先删除数据库
   2)恢复数据 还原db2 数据库中的数据
     先创建数据库表db2
	 source sql文件地址