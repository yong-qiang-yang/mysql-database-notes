 1.MySQL事务的隔离级别
    
	各个事务之间是隔离，相互独立，但是如果多个事务对数据库中的同一书记进行并发访问的时候，就会引发
一些问题，可以通过设置不同的隔离级别来解决对应的问题

   并发访问的问题
	 脏读:一个事务读取到了另一个事务没有提交的数据
     不可重复读: 一个事务中 两次读取的数据不一致。
     幻读: 一个事务中一次查询的结果，无法支撑后续的业务操作。
  
   设置隔离级别
      read uncommitted 读未提交 可以防止哪些问题 无
	  read committed 读已提交 可以防止哪些问题 脏读 (Oracle 默认的隔离级别)
	  repeatable read 可重复读 可以防止 脏读，不可重复读 (MySql 默认的隔离级别)
	  serializable 串行化 可以防止 脏读，不可重复读，幻读
   注意: 隔离级别 从小到大 安全性是越来越高的，但是效率是越来越低的，根据不同的情况选择对应的隔离级别。

 2.隔离级别的相关命令
 
    select @@tx_isolation; -- 查看隔离级别
	set global transaction isolation level 级别名称; -- 设置隔离级别
	read uncommitted 读未提交
	read committed   读已提交
	repeatable read  可重复读
	serializable     串行化
    	  
	-- 设置隔离级别为 读已提交
    set global transaction isolation level read committed ;
 
 3.脏读演示
    
	脏读:一个事务读取到了另一个事务中尚未提交的数据。
	1)打开开窗口登录MySQL，设置全局的隔离级别为最低
     登录是MySql
	 use db2; --使用db2 数据库
	 set global transaction isolation level read uncommitted; -- 设置隔离级别为最低 读未提交
    
    2)关闭窗口,开一个新的窗口A ,再次查询隔离级别
	  开启新的 窗口A
	  select @@tx_isolation; -- 查询隔离级别
	
    3)再开启一个新的窗口 B
	  登录数据库
	  use db2; -- 选择数据库
	  start transaction;  -- 开启事务
	  select * from account; -- 查询
	  
	 (1)A 窗口执行:
	  use db2; -- 选择数据库
	  start transaction;  -- 开启事务
	  -- 执行修改的操作
	  -- tom账户 -500元
      UPDATE account SET money = money - 500 WHERE NAME = 'tom';
      -- jack账户 + 500元
      UPDATE account SET money = money + 500 WHERE NAME = 'jack';
	
	 (2)B 窗口查询数据
	  select * from account -- 查询账户信息
	  
	 (3)A 窗口转账异常，进行回滚
	   rollback;
	
	(4)B 窗口再次查询账户
	   select * from account;
  
 4.解决脏读问题
    
	脏读非常危险的，比如张三向李四购买商品，张三开启事务，向李四账号转入500块，然后打电话给李四说钱
已经转了。李四一查询钱到账了，发货给张三。张三收到货后回滚事务，李四的再查看钱没了。
    解决方案
	   将全局的隔离级别进行提升为:read committed
	 
	1)在A窗口设置全局的隔离级别为read committed
    	set global transaction isolation level read committed; 
	2)重新开启A窗口,查看设置是否成功
	   select @@tx_isolation;
	3)开启B窗口,A和B窗口选择数据库后,都开启事务
	4)A窗口只是更新两个人的账户,不提交事务
	  -- tom账户 -500元
      UPDATE account SET money = money - 500 WHERE NAME = 'tom';
      -- jack账户 + 500元
      UPDATE account SET money = money + 500 WHERE NAME = 'jack';
	5) B窗口进行查询,没有查询到未提交的数据
	  mysql select * from account;
	6)A窗口commit提交数据
	  commit;
	7)B 窗口查看数据
	  select * from account;
 
 5.不可重复演示
    
	不可重复读:同一个事务中,进行查询操作,但是每次读取的数据内容是不一样的。
	1)恢复数据(把数据改回初始状态)
	2)打开两个 窗口A和 窗口B,选择数据库后 开启事务
	  use db2;
	  start transaction;
	  (1) B窗口开启事务后,先进行一次数据查询
	  (2)在A窗口开启事务后，将用户tom的账户 + 500,然后提交事务
	  -- 修改数据
      UPDATE account SET money = money + 500 WHERE NAME = 'tom';
	  -- 提交事务
	  commit;
	3)B 窗口再次查询数据
	  两次查询输出的结果不同，到底哪次是对的。
	  不知道以哪次为准。很多人认为这种情况就对了，无须困惑，当然是后面的为准。
	  比如银行程序需要将查询结果分别输出到电脑屏幕和发短信给客户，结果在一个事务中针对不同的输出
目的地进行的两次查询不一致，导致文件和屏幕中的结果不一致，银行 工作人员就不知道以哪个为准了。

 6.解决不可重复读问题
    
	将全局的隔离级别进行提升为：repeatable read
	1) 恢复数据
	UPDATE account SET money = 1000;
	2) 打开A窗口,设置隔离级别为：repeatable read
	select @@tx_isolation; -- 查看事务的隔离级别
	-- 设置数据的隔离级别为 repeatable read
	set global transaction isolation level repeatable read ;
	3) 重新开启A,B窗口选择数据库,同时开启事务
	4) B窗口事务先进行第一次查询
	select * from account;
    5) A窗口更新数据,然后提交事务
    -- 修改数据
    UPDATE account SET money = money + 500 WHERE NAME = 'tom';
	-- 提交事务
	commit;	
	6)B窗口再次查询
	select * from account;
	同一个事务中为了保证多次查询数据一致，必须使用repeatable read隔离级别。
	
 6.幻读演示
    
	 select某记录是否存在，不存在，准备插入此记录，但执行insert时发现此记录已存在，无法插入，
此时就发生了幻读。

    1)打开A B窗口,选择数据库开启事务
	 use db2;
	 start transaction;
	2)A窗口先执行一次查询操作
	--假设要再添加一条id为3的数据,在添加之前先判断是否存在
	select * from account where id = 3;
	3)B窗口插入一条数据提交事务
	insert into account values(3,'lucy',1000);
	commit;
	4)A窗口执行插入操作,发现报错.出现幻读
    insert into account values(3,'lucy',1000);
	见鬼了，我刚才读到的结果应该可以支持我这样操作才对啊，为什么现在不可以.

 15.解决幻读问题
   
    将事务隔离级别设置到最高SERIALIZABLE，以挡住幻读的发生。
	如果一个事务，使用了SERIALIZABLE——可串行化隔离级别时，在这个事务没有被提交之前,其他的线程
，只能等到当前操作完成之后，才能进行操作，这样会非常耗时，而且，影响数据库的性能，数据库不会使
用这种隔离级别。
    
	1)恢复数据
	delete from account where id = 3;
	2)打开A窗口将数据隔离级别提升到最高
	set global transaction isolation level SERIALIZABLE;
	3)打开A B窗口,选择数据库开启事务
	 use db2;
	 start transaction;
	4)A窗口先执行一次查询操作
	select * from account where id = 3;
	5)B窗口插入一条数据
	insert into account values(3,'lucy',1000);
	6)A窗口执行插入操作,提交事务数据插入成功
	insert into account values(3,'lucy',1000);
	commit;
	7)B窗口在A窗口提交事务之后,再执行,但是主键冲突出现错误。
	总结：
	serializable串行化可以彻底解决幻读,但是事务只能排队执行,严重影响效率，数据库不会使用这种
隔离级别。