 1.事务的概念
   
   事务是一个整体,由一条或者多条SQL语句组成,这些SQL语句要么都执行成功,要么都执行失败,只要有一条SQL出现异常，整个操作就会
回滚,整个业务执行失败。
   
   回滚
   即在事务运行的过程中发生了某种故障，事务不能继续执行，系统将事务中对数据库的所有已完成的操作全部撤销，滚回到事务开始
时的状态。(在提交之前执行)
    
	
	
	CREATE TABLE account(
	-- 主键
	id INT PRIMARY KEY AUTO_INCREMENT,
	-- 姓名
	NAME VARCHAR(10),
	-- 余额
	money DOUBLE
);
--模拟tom给jack转500元钱，一个转账的业务操作最少要执行下面的2条语句
-- 添加两个用户
 INSERT INTO account (NAME, money) VALUES ('tom', 1000), ('jack', 1000);
 
 -- tom账户 -500元
UPDATE account SET money = money - 500 WHERE NAME = 'tom';

出错了

-- jack账户 + 500元
UPDATE account SET money = money + 500 WHERE NAME = 'jack';

 2.MySQL事务操作
   
    /*
  MySql事务操作
       手动提交事务
           1.开启事务 start transaction; 或者 begin;
           2.提交事务 commi;
           3.回滚事务 rollback;
           
           
       自动提交事务
           MySql默认的提交方式 自动提交方式
           每执行一条DML语句 都是一个单独的事务

*/
	  
 3.手动提交事务演示

   -- 成功案例
   USE db2;

   start transaction;  -- 开启事务

   update account set money = money - 500 where name = 'tom'

   update account set money = money + 500 where name = 'jack';
   
   -- 失败案例 会进行回滚
   start transaction; -- 开启事务

   INSERT INTO account VALUES(NULL,'张百万',3000);
   INSERT INTO account VALUES(NULL,'有财',3500);
   
   -- 不去提交事务直接关闭窗口,发生回滚操作,数据没有改变
   -- 如果事务中 SQL 语句没有问题，commit 提交事务，会对数据库数据的数据进行改变。 如果事务中 SQL 语句有问题，
rollback 回滚事务，会回退到开启事务时的状态。 
   
  4.自动提交事务
   
    MySQL默认每一条DML(增删改)语句都是一个单独的事务，每条语句都会自动开启一个事务，语句指向完毕 自动提交事务
MySQL默认开始自动提交事务。
  
 5.自动提交事务演示
    
    1) 将tom账户金额+500
      update account set money = money + 500 where name = 'tom'
    2) 使用SQLYog查看数据库：发现数据已经改变
  
 6.取消自动提交
     
    MySQL默认是自动提交事务,设置为手动提交
     1)登录mysql，查看autocommit状态。
     SHOW VARIABLES LIKE 'autocommit';
       on: 自动提交 off:手动提交
	 2)把 autocommit 改成 off;
     SET @@autocommit=off;
	 3)再次修改，需要提交之后才生效
	   --将jack账户金额 -500 元
	   -- 选择数据库
       use db2;

       -- 修改数据
      update account set money = money - 500 where name = 'jack';

      -- 手动提交
      commit;
 7.事务的四大特性
 
      原子性: 每个事务都是一个整体，不可以再拆分，事务中的所有SQL语句要么都指向成功 要么都执行失败
	  
	  一致性: 事务在执行前数据库的状态与执行后数据库的状态保持一致
	  
	  隔离性: 事务与事务之间不应该相互影响，执行时要保证隔离状态
	  
	  持久性: 一旦事务执行成功，对数据的修改是持久的。

	