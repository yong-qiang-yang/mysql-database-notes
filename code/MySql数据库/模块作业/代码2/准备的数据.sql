CREATE TABLE employee(
id INT PRIMARY KEY AUTO_INCREMENT,
NAME VARCHAR(10),  
gender CHAR(1), -- 性别  
salary DOUBLE,  -- 工资  
bonus DOUBLE,   -- 奖金  
join_date DATE -- 入职日期示
);