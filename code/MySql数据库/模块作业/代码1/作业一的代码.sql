-- 学生表 Student
CREATE TABLE Student(SId VARCHAR(10),Sname VARCHAR(10),Sage DATETIME,Ssex VARCHAR(10));
INSERT INTO Student VALUES('01' , '赵雷' , '1990-01-01' , '男');
INSERT INTO Student VALUES('02' , '钱电' , '1990-12-21' , '男');
INSERT INTO Student VALUES('03' , '孙风' , '1990-12-20' , '男');
INSERT INTO Student VALUES('04' , '李云' , '1990-12-06' , '男');
INSERT INTO Student VALUES('05' , '周梅' , '1991-12-01' , '女');
INSERT INTO Student VALUES('06' , '吴兰' , '1992-01-01' , '女');
INSERT INTO Student VALUES('07' , '郑竹' , '1989-01-01' , '女');
INSERT INTO Student VALUES('09' , '张三' , '2017-12-20' , '女');
INSERT INTO Student VALUES('10' , '李四' , '2017-12-25' , '女');
INSERT INTO Student VALUES('11' , '李四' , '2012-06-06' , '女');
INSERT INTO Student VALUES('12' , '赵六' , '2013-06-13' , '女');
INSERT INTO Student VALUES('13' , '孙七' , '2014-06-01' , '女');

-- 科目表 Course
CREATE TABLE Course(CId VARCHAR(10),Cname NVARCHAR(10),TId VARCHAR(10));
INSERT INTO Course VALUES('01' , '语文' , '02');
INSERT INTO Course VALUES('02' , '数学' , '01');
INSERT INTO Course VALUES('03' , '英语' , '03');

-- 教师表 Teacher
CREATE TABLE Teacher(TId VARCHAR(10),Tname VARCHAR(10));
INSERT INTO Teacher VALUES('01' , '张三');
INSERT INTO Teacher VALUES('02' , '李四');
INSERT INTO Teacher VALUES('03' , '王五');

-- 成绩表 SC
CREATE TABLE SC(SId VARCHAR(10),CId VARCHAR(10),score DECIMAL(18,1));
INSERT INTO SC VALUES('01' , '01' , 80);
INSERT INTO SC VALUES('01' , '02' , 90);
INSERT INTO SC VALUES('01' , '03' , 99);
INSERT INTO SC VALUES('02' , '01' , 70);
INSERT INTO SC VALUES('02' , '02' , 60);
INSERT INTO SC VALUES('02' , '03' , 80);
INSERT INTO SC VALUES('03' , '01' , 80);
INSERT INTO SC VALUES('03' , '02' , 80);
INSERT INTO SC VALUES('03' , '03' , 80);
INSERT INTO SC VALUES('04' , '01' , 50);
INSERT INTO SC VALUES('04' , '02' , 30);
INSERT INTO SC VALUES('04' , '03' , 20);
INSERT INTO SC VALUES('05' , '01' , 76);
INSERT INTO SC VALUES('05' , '02' , 87);
INSERT INTO SC VALUES('06' , '01' , 31);
INSERT INTO SC VALUES('06' , '03' , 34);
INSERT INTO SC VALUES('07' , '02' , 89);
INSERT INTO SC VALUES('07' , '03' , 98);


-- 1.查询在 SC 表存在成绩的学生信息
SELECT DISTINCT
st.`SId`,
st.`Sname`,
st.`Sage`,
st.`Ssex`
FROM SC sc LEFT JOIN Student st ON sc.`SId` = st.`SId`
WHERE sc.`score` IS NOT NULL;
-- 2.查询「李」姓老师的数量
SELECT 
COUNT(t.`Tname`)
FROM Teacher t WHERE t.`Tname` LIKE "李%";
-- 3.查询学过「张三」老师授课的同学的信息  多表联合查询
SELECT 
st.`SId`,
st.`Sname`,
st.`Sage`,
st.`Ssex`
FROM Teacher te INNER JOIN Course cs ON te.`TId` = cs.`TId`
INNER JOIN SC sc ON cs.`CId` = sc.`CId`
INNER JOIN Student st ON sc.`SId` = st.`SId`
WHERE te.`Tname` = '张三';
-- 4.查询男生、女生人数
SELECT COUNT(st.`Ssex`) FROM Student st WHERE st.`Ssex`= '男';
SELECT COUNT(st.`Ssex`) FROM Student st WHERE st.`Ssex`= '女';
-- 5.查询 1990 年出生的学生名单
SELECT * FROM Student st WHERE st.`Sage` LIKE '1990%';
-- 6.查询每门课程的平均成绩，结果按平均成绩降序排列，平均成绩相同时，按课程编号升序排列
SELECT 
cs.`CId`,
cs.`Cname`,
AVG(sc.`score`)
FROM SC sc INNER JOIN Course cs ON sc.`CId` = cs.`CId`
GROUP BY cs.`Cname` 
ORDER BY AVG(sc.`score`) DESC,cs.`CId` ASC;
-- 7.查询课程编号为 01 且课程成绩在 80 分及以上的学生的学号和姓名 
SELECT
st.`SId`,
st.`Sname`
FROM Course cs INNER JOIN SC sc ON cs.`CId` = sc.`CId`
INNER JOIN Student st ON sc.`SId` = st.`SId`
WHERE cs.`CId` = '01' AND sc.`score` >= 80;
-- 8.查询" 01 "课程比" 02 "课程成绩高的学生的信息及课程分数
SELECT
st.`SId`,
st.`Sname`,
st.`Sage`,
st.`Ssex`,
sc1.`score`
FROM Student st INNER JOIN
(SELECT 
cs.`CId`,
sc.`SId`,
sc.`score`
FROM Course cs INNER JOIN SC sc ON cs.`CId` = sc.`CId`
WHERE cs.`CId` = '01') sc1 ON st.`SId` = sc1.`SId`
INNER JOIN
(SELECT
cs.`CId`, 
sc.`SId`,
sc.`score`
FROM Course cs INNER JOIN SC sc ON cs.`CId` = sc.`CId`
WHERE cs.`CId` = '02') sc2 ON st.`SId` = sc2.`SId`
WHERE sc1.`score`> sc2.`score`;



