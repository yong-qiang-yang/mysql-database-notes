 1.JDBC的概念
  
  JDBC(Java Data Base Connectivity)是Java访问数据库的标准规范.是一种用于执行SQL语句的Java API，可以为多种关系数据库提供统一访问，
它由一组用Java语言编写的类和接口组成。是Java访问数据库的标准规范。
 
 2.JDBC原理
  
  JDBC是接口，驱动是接口的实现，没有驱动将无法完成数据库连接，从而不能操作数据库！每个数据库厂商都需要提供自己的驱动，用来连接
自己公司的数据库，也就是说驱动一般都由数据库生成厂商提供。
  总结：
  JDBC就是由sun公司定义的一套操作所有关系型数据库的规则(接口)，而数据库厂商需要实现这套接口,提供数据库驱动jar包,我们可以使用这套
接口编程,真正执行的代码是对应驱动包中的实现类。

 3.数据准备

 -- 创建 jdbc_user表
CREATE TABLE jdbc_user(
  id INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(50),
  PASSWORD VARCHAR(50),
  birthday DATE
);

-- 添加数据
INSERT INTO jdbc_user (username,PASSWORD,birthday)
VALUES('admin1', '123','1991/12/24'),
('admin2', '123','1995/12/24'),
('test1', '123','1998/12/24'),
('test2', '123','2000/12/24');

 4.MySql驱动包
 
 将MySQL驱动包添加到jar包库文件夹中，Myjar文件夹,用于存放当前项目需要的所有jar包。
 
 5.注册驱动
 
   JDBC规范定义驱动接口：java.sql.Driver
   MySql驱动包提供了实现类：com.mysql.jdbc.Driver
   加载注册驱动的方式
   Class.forName(数据库驱动实现类) 
   概述：加载和注册数据库驱动,数据库驱动由数据库厂商MySql提供(com.mysql.jdbc.Driver)

 Driver类是由MySql驱动包提供的一个实现类 他实现了 java.sql.Driver
 public class JDBCDemo01 {

    public static void main(String[] args) throws Exception {

        // 1.注册驱动
		// forName 方法执行将类进行初始化
        Class.forName("com.mysql.jdbc.Driver");

    }
}
 public class Driver extends NonRegisteringDriver implements java.sql.Driver {
    public Driver() throws SQLException {
    }
    静态代码块 随着类的加载而加载 只加载一次
    static {
        try {
			DriverManager 类就是驱动管理类 registerDriver 方法是用来注册驱动的
            DriverManager.registerDriver(new Driver());
        } catch (SQLException var1) {
            throw new RuntimeException("Can't register driver!");
        }
    }
}

 6.获取连接
 
  Connection接口，代表一个连接对象,具体的实现类由数据库的厂商实现。
  使用DriverManager类的静态方法,getConnection可以获取数据库的连接。
  获取连接的静态方法 Connection getConnection(String url, String user,String password)
  说明 通过连接字符串和用户名,密码来获取数据库连接对象
  1) getConnection方法3个连接参数说明
  user     -- 登录用户名
  password -- 登录密码
  url      -- mySql URL的格式 jdbc:mysql://localhost:3306/db4
  2) 对URL的详细说明
  jdbc:mysql://localhost:3306/db4?characterEncoding=UTF-8
  jdbc 是协议 mysql是子协议就是数据库名称 localhost:3306 主机名:端口号 db4 数据库
  characterEncoding=UTF-8 参数 = 参数值
  
  public class JDBCDemo01 {

    public static void main(String[] args) throws Exception {

        // 1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        // 2.获取连接
        String url = "jdbc:mysql://localhost:3306/db4?characterEncoding=UTF-8";
        Connection con = DriverManager.getConnection(url, "root", "123456");

        // 打印 连接对象
        System.out.println(con);  // com.mysql.jdbc.JDBC4Connection@4524411f


    }
	
  7.获取语句执行平台
  
   通过Connection的createStatement方法获取sql语句执行对象
   Connection接口中的方法                说明
   Statement createStatement()    创建SQL语句执行对象
   
   Statement：代表一条语句对象，用于发送SQL语句给服务器，用于执行静态SQL语句并返回它所生成结果的对象
   Statement类常用方法                                    说明
   int executeUpdate(String                执行insert update delete语句
   sql)                                    返回int类型,代表受影响的行数
   ResultSet executeQuery(String                     执行select语句
   sql)                                          返回ResultSet结果集对象 
   
   public class JDBCDemo01 {

    public static void main(String[] args) throws Exception {

        // 1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        // 2.获取连接
        String url = "jdbc:mysql://localhost:3306/db4?characterEncoding=UTF-8";
        Connection con = DriverManager.getConnection(url, "root", "123456");

        // 打印 连接对象
        System.out.println(con);  // com.mysql.jdbc.JDBC4Connection@4524411f

        // 3.获取语句执行平台 Statement
        Statement statement = con.createStatement();

        // 3.1 通过 statement对象的 executeUpdate 方法 创建一张表
        String sql = "create table test(id int,name varchar(20),age int);";
        // 3.2 增删改操作 使用executeUpdate，增加一张表
        int i = statement.executeUpdate(sql); // 返回值是int类型 表示受影响的行数

        System.out.println(i);
        // 关闭流
        statement.close();
        con.close();

    }
}

 8.处理结果集
  
   只有在进行查询操作的时候,才会处理结果集。
   
   public class JDBCDemo02 {

    public static void main(String[] args) throws Exception {

        // 1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        // 2.获取连接
        String url = "jdbc:mysql://localhost:3306/db4?characterEncoding=UTF-8";
        Connection con = DriverManager.getConnection(url, "root", "123456");

        // 3.获取语句执行平台对象
        Statement statement = con.createStatement();

        // 4.执行查询操作 使用executeQuery()
        String sql = "select * from jdbc_user";
        // 执行查询操作,返回的是一个ResultSet结果对象
        ResultSet resultSet = statement.executeQuery(sql);
        // 处理结果集

    }
}

 9.ResultSet接口
  
  作用：封装数据库查询的结果集，对结果集进行遍历，取出每一条记录。
  
   ResultSet接口方法                             说明
   boolean next()                             游标向下一行
                           返回boolean类型，如果还有下一条记录，返回true，否则返回false
   xxx getXxx( String           通过列名，参数是String类型。返回不同的类型
   or int)                     通过列号，参数是整数，从1开始。返回不同的类型
   
   int getInt("列名");         int getInt(1)       -- 表示列号
   String getString("列名")    String getString(1) -- 表示列号
   
   public class JDBCDemo02 {

    public static void main(String[] args) throws Exception {

        // 1.注册驱动
        Class.forName("com.mysql.jdbc.Driver");
        // 2.获取连接
        String url = "jdbc:mysql://localhost:3306/db4?characterEncoding=UTF-8";
        Connection con = DriverManager.getConnection(url, "root", "123456");

        // 3.获取语句执行平台对象
        Statement statement = con.createStatement();

        // 4.执行查询操作 使用executeQuery()
        String sql = "select * from jdbc_user";
        // 执行查询操作,返回的是一个ResultSet结果对象
        ResultSet resultSet = statement.executeQuery(sql);

        // resultSet 处理结果集
//        boolean next = resultSet.next(); // 判断是否有下一条数据
//        System.out.println(next);

        //获取id
        //int id = resultSet.getInt("id");
        //System.out.println("通过列名的方式获取" + id);

//        int id = resultSet.getInt(1);
//        System.out.println("通过列号的方式获取" + id);

        //通过while循环 遍历获取resultSet中的数据
        while(resultSet.next()){
            //获取id
            int id = resultSet.getInt("id");

            //获取姓名
            String username = resultSet.getString("username");

            //获取密码
            String password = resultSet.getString("password");

            //获取生日
            Date birthday = resultSet.getDate("birthday");

            System.out.println(id + ":" + username + ":" + password + ":" + birthday);
        }
        // 5.关闭流操作
        resultSet.close();
        statement.close();
        con.close();

    }
}

 10.释放资源
 
   1)需要释放的对象：ResultSet结果集，Statement语句，Connection连接
   2)释放原则：先开的后关，后开的先关。ResultSet ==> Statement ==> Connection
   3)放在哪个代码块中：finally块
     与IO流一样，使用后的东西都需要关闭！关闭的顺序是先开后关,先得到的后关闭，后得到的先关闭
	 
	 public class JDBCDemo03 {

    public static void main(String[] args) {

        Connection con = null;
        Statement statement = null;
        ResultSet resultSet = null;

        // 获取连接
        String url = "jdbc:mysql://localhost:3306/db4?characterEncoding=UTF-8";
        try {
            //1.注册驱动 省略
            //2.获取连接
            con = DriverManager.getConnection(url, "root", "123456");
            //3.获取语句执行平台对象
            statement = con.createStatement();
            //4.执行sql
            String sql = "select * from jdbc_user";
            resultSet = statement.executeQuery(sql);

            //5.处理结果集对象
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
             //finally 中的代码始终会执行
            try {
                resultSet.close();
                statement.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
}

 11.步骤总结
   1)获取驱动(可以省略)
   2)获取连接
   3)获取Statement对象
   4)处理结果集(只在查询时处理)
   5)释放资源

