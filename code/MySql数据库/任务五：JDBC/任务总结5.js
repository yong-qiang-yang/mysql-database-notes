 1.JDBC概述
    
  1)什么是JDBC(java访问数据库的标准规范)
  2)JDBC的作用(统一了访问数据库的标准规范，使我们使用一套代码就可以操作所有的关系型数据库)
  3)数据库驱动(JDBC是一套接口，具体的实现类有数据库厂商提供，实现类被封装成了jar，这个jar包就是数据库驱动)
 
 2.JDBC开发(重点)
  
  1)注册驱动(可以省略) Class.forName("com.mysql.jdbc.Driver");
  2)获取连接 Connection
  3)获取语句执行平台 Statement
  4)处理结果集 resultSet 只有查询有结果集
  5)释放资源 顺序resultSet ===> Statement ===> Connection
 
 3.SQL注入问题
  
  1)什么是SQL注入(用户输入的内容进行了修改 成为了SQL语句的一部分，改变了原来的含义，就被称为SQL注入)
  select * from jdbc_user where usersname = 'tom' and password = '123' or '1' = '1';
  2)如何解决(使用PreparedStatement预处理对象解决)
 
 4.预处理对象(重点)
 
  1)什么是预处理对象(PreparedStatement是Statement接口的子接口,他是一个预编译sql对象)
  2)作用:
   有预编译的功能，可以防止sql注入
   提高SQL的执行效率
  3)预处理对象的使用方式(使用？占位符的方式，去动态的设置参数)
   select * from jdbc_user where username = ? and password = ?;
  4)statement与PreparedStatement的区别
  Statement用于执行静态SQL语句
  PrepareStatement是预编译的SQL语句对象,可以使用？占位符的方式，去动态的设置参数

 5.JDBC控制事务
  
  1)开启事务 setAutoCommit(false)
  2)提交事务 commit()
  3)回滚事务 rollback()
  