 1.Sql注入演示
  
  1)向jdbc_user表中 插入两条数据
  # 插入2条数据
  insert into jdbc_user values(null,'jack','123456','2020/2/24');
  insert into jdbc_user values(null,'tom','123456','2020/2/24');
  2)SQL 注入演示
  # SQL注入演示
  -- 填写一个错误的密码
  select * from jdbc_user where usersname = 'tom' and password = '123' or '1' = '1';
  如果这是一个登陆操作,那么用户就登陆成功了.显然这不是我们想要看到的结果。
  
 2.sql注入案例：用户登陆
  
  需求:用户在控制台上输入用户名和密码,然后使用Statement字符串拼接的方式实现用户的登录
  步骤：
      1)得到用户从控制台上输入的用户名和密码来查询数据库
	  2)写一个登录的方法
	   a)通过工具类得到连接
	   b)创建语句对象，使用拼接字符串的方式生成SQL语句
	   c)查询数据库，如果有记录则表示登录成功，否则登录失败
	   d)释放资源
	   Sql注入方式:123' or '1'= '1'

public class TestLogin01 {

    /**
     * 用户登录 案列
     * @param args
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException {

        //1.获取连接
        Connection con = JDBCUtils.getConnection();
        //2.获取Statement对象
        Statement statement = con.createStatement();
        //3.获取用户输入的用户名和密码
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入用户名:");
        String name = sc.nextLine();
        System.out.println("请输入密码:");
        String pass = sc.nextLine();
        //4.拼接SQL语句
        String sql = "select * from jdbc_user where username = '" + name + "' and password = '" + pass +"'";
        System.out.println(sql);
        //5.执行查询 获取结果集对象
        ResultSet resultSet = statement.executeQuery(sql);

        //6.处理结果
        if(resultSet.next()){
            System.out.println("登录成功！ 欢迎你:" + name);
        }else {
            System.out.println("登录失败");
        }

        //7.释放资源
        JDBCUtils.close(con,statement,resultSet);
    }


}
 3.问题分析
  
   什么是sql注入：我们让用户输入的密码和SQL语句进行字符串拼接。用户输入的内容作为了SQL语句语法的一部分，改变了
原有SQL真正的意义，以上问题称为SQL注入。
  如何实现的注入
  根据用户输入的数据,拼接处的字符串
  select * from jdbc_user where username = 'abc' and password = 'abc' or '1' = '1'
  相当于select * from user where true = true; 查询了所有记录
  如何解决
  要解决SQL注入就不能让用户输入的密码和我们的SQL语句进行简单的字符串拼接。

  