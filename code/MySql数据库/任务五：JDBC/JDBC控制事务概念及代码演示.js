 1.JDBC控制事务
  
  之前我们是使用MySQL的命令来操作事务。接下来我们使用JDBC来操作银行转账的事务。
 
 2.数据准备
  
  -- 创建账户表
CREATE TABLE account(
  -- 主键
  id INT PRIMARY KEY AUTO_INCREMENT,
  -- 姓名
  NAME VARCHAR(10),
  -- 转账金额
  money DOUBLE
);

-- 添加两个用户
INSERT INTO account(NAME,money) VALUES('tom', 1000), ('jack', 1000);

 3.事务相关API
  
  我们使用Connection中的方法实现事务管理
   方法                                            说明
  void setAutoCommit(boolean          参数是true或false如果设置为false
  autoCommit)                         表示关闭自动提交，相当于开启事务
  void commit()                                  提交事务
  void rollback()                                回滚事务
 
 4.开发步骤
 
  1)获取连接
  2)开启事务
  3)获取预处理 执行SQL (两次修改操作)
  4)提交事务(正常情况)
  5)出现异常回滚事务
  6)释放资源 
  代码示例
  public class JDBCTransaction {

    public static void main(String[] args) {

        Connection connection = null;
        PreparedStatement ps = null;
        try {
            //1.获取连接
            connection = JDBCUtils.getConnection();
            //2.开启事务
            connection.setAutoCommit(false); // 手动提交事务
            //3.获取预处理 执行SQL (两次修改操作)
            //3.1 tom 账户 -500
            ps = connection.prepareStatement("update account set money = money - ? where name = ?");
            ps.setDouble(1,500.0);
            ps.setString(2,"tom");
            ps.executeUpdate();

            // 模拟 tom转账之后出现异常
            //System.out.println(1 / 0);
            
            //3.1 jack 账户 +500
            ps = connection.prepareStatement("update account set money = money + ? where name = ?");
            ps.setDouble(1,500.0);
            ps.setString(2,"jack");
            ps.executeUpdate();
            //4.提交事务(正常情况)
            connection.commit();
            System.out.println("转账成功");
            //6.释放资源

        } catch (SQLException e) {
            e.printStackTrace();
            //5.出现异常回滚事务
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            //6.释放资源
            JDBCUtils.close(connection,ps);
        }
    }
}