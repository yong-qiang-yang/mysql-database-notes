 1.JDBC工具类的概念
 
  什么时候自己创建工具类？
  如果一个功能经常要用到，我们建议把这个功能做成一个工具类，可以在不同的地方重用。
  "获得数据库连接"操作，将在以后的增删改查所有功能中都存在，可以封装工具类JDBCUtils,提供获取连接对象的方法，从而达到
代码的重复利用。
  工具类包含的内容
  1)可以把几个字符串定义成常量：用户名，密码，URL，驱动类
  2)得到数据库的连接：getConnection()
  3)关闭所有打开的资源
  
  /**
 *  JDBC工具类
 */
public class JDBCUtils {

    //1.将连接信息定义为 字符串常量
    public static final String DRIVERNAME = "com.mysql.jdbc.Driver";
    public static final String URL = "jdbc:mysql://localhost:3306/db4?characterEncoding=UTF-8";
    public static final String USER = "root";
    public static final String PASSWORD = "123456";

    //2.静态代码块
    static{
        //1.注册驱动
        try {
            Class.forName(DRIVERNAME);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    //3.获取连接的静态方法
    public static Connection getConnection(){
        //获取连接对象 并返回
        try {
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            return connection;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

    //4.关闭资源的方法
    public static void close(Connection con, Statement statement){
        if(con != null && statement != null){
            try {
                statement.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }


    public static void close(Connection con, Statement statement, ResultSet resultSet){
        if(con != null && statement != null){
            try {
                statement.close();
                con.close();
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }
}
 2.DML操作
 
    插入记录：解决插入中文乱码问题
	jdbc:mysql://localhost:3306/db4?characterEncoding=UTF-8  指定字符的编码、解码格式
	
	/**
     * 插入数据
     */
    @Test
    public void testInsert() throws SQLException {

        // 1.通过JDBCUtils工具类 获取连接
        Connection con = JDBCUtils.getConnection();

        //2.获取Statement对象
        Statement statement = con.createStatement();

        //2.1 编写SQL
        String sql = "insert into jdbc_user values(null,'张百万',123,'2020/11/11')";

        //2.2 执行SQL
        int i = statement.executeUpdate(sql);
        System.out.println(i);
        //3.关闭流
        JDBCUtils.close(con,statement);
    }

	更新记录
	根据ID需改用户名称
	
	/**
     *  更新操作 根据id修改用户名
     */
    @Test
    public void testUpdate() throws SQLException {

        Connection con = JDBCUtils.getConnection();

        Statement statement = con.createStatement();

        String sql = "update jdbc_user set username = '刘能' where id = 1";

        statement.executeUpdate(sql);

        JDBCUtils.close(con,statement);
    }
	删除记录
	删除id为1和2的记录
	
	/**
     * 删除操作
     * 删除id 为 1 和 2 的数据
     * @throws SQLException
     */
    @Test
    public void testDelete() throws SQLException {

        Connection con = JDBCUtils.getConnection();

        Statement statement = con.createStatement();

        String sql = "delete from jdbc_user where id in(1,2)";

        statement.executeUpdate(sql);

        JDBCUtils.close(con,statement);
    }
	
   查询姓名为张百万的一条记录
   public class TestDQL {

    // 查询姓名为张百万的一条记录
    public static void main(String[] args) throws SQLException {
        //1.获取连接
        Connection connection = JDBCUtils.getConnection();

        //2.创建Statement 对象
        Statement statement = connection.createStatement();

        //3.编写SQL
        String sql = "select * from jdbc_user where username = '张百万'";
        ResultSet resultSet = statement.executeQuery(sql);

        //4.处理结果集
        while (resultSet.next()){
            int id = resultSet.getInt("id");
            String username = resultSet.getString("username");
            String password = resultSet.getString("password");
            Date birthday = resultSet.getDate("birthday");

            System.out.println(id + ":" + username + ":" + password + ":" + birthday);

        }

        //5.释放资源
        JDBCUtils.close(connection,statement,resultSet);

    }
}
   