 1.预处理对象PreparedStatement的概念
   
   PreparedStatement是Statement接口的子接口，继承于父接口中所有的方法。它是一个预编译的SQL语句对象
   预编译：是指SQL语句被预编译,并存储在PreparedStatement对象中。然后可以使用此对象多次高效地执行该语句。
 
 2.PreparedStatement特点
  
   因为有预先编译的功能，提高SQL的执行效率。
   可以有效的防止SQL注入的问题，安全性更高。
 
 3.获取PreparedStatement对象
   
   通过Connection创建PreparedStatement对象
    Connection接口中的方法                           说明
	PreparedStatement prepareStatement       指定预编译的SQL语句
	(String sql)                        SQL语句中使用占位符?创建一个语句对象
	
 4.PreparedStatement接口常用方法
 
   常用方法                                         说明
   int executeUpdate()                  执行insert update delete语句
   ResultSet executeQuery()          执行select语句.返回结果集对象Resulet
   
 5.使用PreparedStatement的步骤
  
   1)编写SQL语句，未知内容使用?占位
   2)获得PreparedStatement对象
   3)设置实际参数：setXxx(占位符的位置,真实的值)
   4)执行参数化 sql语句
   5)关闭资源
  
    setXxx重载方法                                                    说明
    void setDouble(int parameterIndex, double x)	   将指定参数设置为给定Java double值
	void setInt(int parameterIndex, int x)              将指定参数设置为给定Java int值
	void setString(int parameterIndex, String x)       将指定参数设置为给定Java String值
	void setObject(int parameterIndex, Object x)       使用给定对象设置指定参数的值

 6.使用PreparedStatement完成登录案例
  
  使用PreparedStatement预处理对象,可以有效的避免SQL注入。
  public class TestLogin02 {

    /**
     *  SQL 注入
     *     用户输入的用户名和面 与我们编写的sql进行了拼接，用户输入的内容成为了sql语法的一部分
     *     用户会利用这里漏洞 输入一些其他的字符串，改变sql原有的意思
     *  如何解决
     *  要解决SQL注入就不能让用户输入的密码和我们的SQL语句进行简单的字符串拼接。
     *
     *  PreparedStatement是Statement接口的子接口
     *   使用预处理对象 他有预编译功能，提高sql的执行效率
     *   使用预处理对象 通过占位符的方式 设置参数 可以有效的防止sql注入
     * @param args
     */
    public static void main(String[] args) throws SQLException {

        //1.获取连接
        Connection con = JDBCUtils.getConnection();
        //2.获取Statement对象
        //3.获取用户输入的用户名和密码
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入用户名:");
        String name = sc.nextLine();
        System.out.println("请输入密码:");
        String pass = sc.nextLine();
        //4.获取 PrepareStatement

        //4.1编写SQL 使用？ 占位符方式
        String sql = "select * from jdbc_user where username = ? and password = ?";
        PreparedStatement preparedStatement = con.prepareStatement(sql);
        //4.2 设置占位符参数
        preparedStatement.setString(1,name);
        preparedStatement.setString(2,pass);
        //5.执行查询 处理结果集
        ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet.next()){
            System.out.println("登录成功！ 欢迎你:" + name);
        }else {
            System.out.println("登录失败");
        }
        //6.释放资源
        JDBCUtils.close(con,preparedStatement,resultSet);

    }
}
 7.PreparedStatement的执行原理
    分别使用Statement对象和PreparedStatement对象进行插入操作
	public class TestPS {

    public static void main(String[] args) throws SQLException {

        Connection connection = JDBCUtils.getConnection();

        //获取Statement对象
        Statement statement = connection.createStatement();

        //向数据库插入两条数据
        statement.executeUpdate("insert into jdbc_user values(null,'张三','123456','2000/12/26')");
        statement.executeUpdate("insert into jdbc_user values(null,'李四','654321','1900/12/26')");

        PreparedStatement ps = connection.prepareStatement("insert into jdbc_user values(?,?,?,?)");

        // 先插入一条数据
        ps.setObject(1,null);
        ps.setString(2,"小斌");
        ps.setString(3,"qwer");
        ps.setString(4,"1999/11/11");
        // 执行插入
        ps.executeUpdate();

        // 先插入一条数据
        ps.setObject(1,null);
        ps.setString(2,"长海");
        ps.setString(3,"asdf");
        ps.setString(4,"2000/11/11");
        // 执行插入
        ps.executeUpdate();

        ps.close();
        statement.close();
        connection.close();



    }
}

 8.statement与PreparedStatement的区别
   
   1)Statement用于执行静态SQL语句，在执行时，必须指定一个事先准备好的SQL语句
   2)PrepareStatement是预编译的SQL语句对象，语句中可以包含动态参数"?"，在执行时可以为"?"动态设置参数值
   3)PrepareStatement可以减少编译次数提高数据库性能.