 1.连接池的概念
 
  实际开发中"获得连接"或"释放资源"是非常消耗系统资源的两个过程，为了解决此类性能问题，通常情况我们采用连接池技术,来共享
连接Connection。这样我们就不需要每次都创建连接、释放连接了，这些操作都交给了连接池.
  连接池的好处：用池来管理Connection，这样可以重复使用Connection。当使用完Connection后，调用Connection的close()方法也不
会真的关闭关闭Connection，而是把Connection“归还”给池。
 
 2.如何使用数据库连接池
   
   Java为数据库连接池提供了公共的接口：javax.sql.DataSource，各个厂商需要让自己的连接池实现这个接口。这样应用程序可以方便
的切换不同厂商的连接池!。
   常见的连接池有DBCP连接池, C3P0连接池, Druid连接池。
 
 3.数据准备
 
   #创建数据库
CREATE DATABASE db5 CHARACTER SET utf8;

#使用数据库
USE db5;

#创建员工表
CREATE TABLE employee(
 eid INT PRIMARY KEY AUTO_INCREMENT,
 ename VARCHAR(20), -- 员工姓名
 age INT,           -- 员工年龄
 sex VARCHAR(6),    -- 员工性别
 salary DOUBLE,     -- 薪水
 empdate DATE       -- 入职日期
);

#插入数据
INSERT INTO employee (eid, ename, age, sex, salary, empdate)
VALUES(NULL,'李清照',22,'女',4000,'2018-11-12');
INSERT INTO employee (eid, ename, age, sex, salary, empdate)
VALUES(NULL,'林黛玉',20,'女',5000,'2019-03-14');
INSERT INTO employee (eid, ename, age, sex, salary, empdate)
VALUES(NULL,'杜甫',40,'男',6000,'2020-01-01');
INSERT INTO employee (eid, ename, age, sex, salary, empdate)
VALUES(NULL,'李白',25,'男',3000,'2017-10-01');

 4.DBCP连接池
  
  DBCP也是一个开源的连接池，是Apache成员之一，在企业开发中也比较常见，tomcat内置的连接池。
 
 5.编写工具类
  
  连接数据库表的工具类,采用DBCP连接池的方式来完成
  Java中提供了一个连接池的规则接口：DataSource,它是java中提供的连接池
  在DBCP包中提供了DataSource接口的实现类，我们要用的具体的连接池BasicDataSource类。
  public class DBCPUtils {

    //1.定义常量 保存数据库连接的相关信息
    public static final String DRIVERNAME = "com.mysql.jdbc.Driver";
    public static final String URL = "jdbc:mysql://localhost:3306/db5?characterEncoding=UTF-8";
    public static final String USER = "root";
    public static final String PASSWORD = "123456";

    //2.创建连接池对象
    public static BasicDataSource dataSource = new BasicDataSource();

    //3.静态代码块进行配置
    static{
        dataSource.setDriverClassName(DRIVERNAME);
        dataSource.setUrl(URL);
        dataSource.setUsername(USER);
        dataSource.setPassword(PASSWORD);
    }

    //4.获取连接的方法
    public static Connection getConnection() throws SQLException {

        //从连接池获取连接
        Connection connection = dataSource.getConnection();
        return  connection;
    }
    //5.释放资源方法
    public static void close(Connection con, Statement statement){
        if(con != null && statement != null){
            try {
                statement.close();
                //归还连接
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Connection con, Statement statement, ResultSet resultSet){
        if(con != null && statement != null && resultSet !=null){
            try {
                resultSet.close();
                statement.close();
                //归还连接
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
  6.测试工具类
    
	需求:查询所有员工的姓名
	public class TestDBCP {

    /**
     * 测试DBCP连接池
     */
    public static void main(String[] args) throws SQLException {

        //1.从DBCP连接池中拿到连接
        Connection con = DBCPUtils.getConnection();

        //2.获取Statement对象
        Statement statement = con.createStatement();

        //3.查询所有员工的姓名
        String sql = "select ename from employee";
        ResultSet resultSet = statement.executeQuery(sql);

        //4.处理结果集
        while (resultSet.next()){
            String ename = resultSet.getString("ename");
            System.out.println("员工姓名: " + ename);
        }

        //5.释放资源
        DBCPUtils.close(con,statement,resultSet);

    }
}
 7.常见配置项
  
   属性                                  描述
   driverClassName                   数据库驱动名称
   url                                 数据库地址
   username                             用户名
   password                              密码
   maxActive                          最大连接数量
   maxIdle                            最大空闲连接
   minIdle                            最小空闲连接
   initialSize                         初始化连接
   
