 1.ResultSetHandler接口的简介
  
  ResultSetHandler可以对查询出来的ResultSet结果集进行处理，达到一些业务上的需求。
 
 2.ResultSetHandler结果集处理类
 
  本例展示的是使用ResultSetHandler接口的几个常见实现类实现数据库的增删改查，可以大大减少代码量，优化程序。
  每一种实现类都代表了对查询结果集的一种处理方式
  ResultSetHandler实现类                        说明
  ArrayHandler -- 将结果集中的第一条记录封装到一个Object[]数组中，数组中的每一个元素就是这条记录中的每一个字段的值
  ArrayListHandler -- 将结果集中的每一条记录都封装到一个Object[]数组中，将这些数组在封装到List集合
  BeanHandler -- 将结果集中第一条记录封装到一个指定的javaBean中
  BeanListHandler -- 将结果集中每一条记录封装到指定的javaBean中，再将这些javaBean在封装到List集合中
  ColumnListHandler -- 将结果集中指定的列的字段值，封装到一个List集合中
  KeyedHandler -- 将结果集中每一条记录封装到Map<String,Object>,在将这个map集合做为另一个Map的value,另一个Map集合的
                  key是指定的字段的值。
  MapHandler -- 将结果集中第一条记录封装到了Map<String,Object>集合中，key就是字段名称，value就是字段值。
  MapListHandler -- 将结果集中每一条记录封装到了Map<String,Object>集合中，key就是字段名称value就是字段值，在将这些
                    Map封装到List集合中。
  ScalarHandler -- 它是用于封装单个数据。例如 select count(*) from 表操作
 
 3.ResultSetHandler常用实现类测试
  
  QueryRunner的查询方法
  query方法的返回值都是泛型,具体的返回值类型,会根据结果集的处理方式,发生变化。
   方法                                                           说明
  query(String sql, handler ,Object[] param)      自动模式创建QueryRunner,执行查询<b
  query(Connection con,String sql,handler,        手动模式创建QueryRunner,执行查询
  Object[] param)
   创建一个测试类,对ResultSetHandler接口的几个常见实现类进行测试
   1).查询id为5的记录,封装到数组中
   public class DBUtilsDemo03 {

    /*
    *  查询id为5的记录,封装到数组中
    *  ArrayHandler 将结果集的第一条数据封装到数组中
    **/
    @Test
    public void testFindById() throws SQLException {

        //1.创建QueryRunner
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        //2.编写SQL
        String sql = "select * from employee where eid = ?";

        //3.执行查询
        Object[] param = qr.query(sql, new ArrayHandler(), 5);

        //4.获取数据
        System.out.println(Arrays.toString(param));
    }   
   
   2).查询所有数据，封装到List集合中
   
   /**
     *  查询所有数据，封装到List集合中
     *  ArrayListHandler可以将每条数据先封装到数组中，再将数组封装到集合中
     */
    @Test
    public void testFindAll() throws SQLException {

        //1.创建QueryRunner
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());
        
        //2.编写SQL
        String sql = "select * from employee";
        
        //3.执行查询
        List<Object[]> query = qr.query(sql, new ArrayListHandler());

        //4.遍历集合获取数据
        for (Object[] objects : query){
            System.out.println(Arrays.toString(objects));
        }
    }
   3).查询id为5的记录，封装到指定的JavaBean中
   
   /**
     * 查询id为5的记录，封装到指定的JavaBean中
     *  BeanHandler 将结果的第一条数据封装到 JavaBean中
     *
     */
    @Test
    public void testFindByIdJavaBean() throws SQLException {

        //1.创建QueryRunner
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        //2.编写SQL
        String sql = "select * from employee where eid = ?";

        Employee  employee= qr.query(sql,new BeanHandler<Employee>(Employee.class),3);
        System.out.println(employee);
    }
   4).查询薪资大于 3000 的所员工信息,封装到JavaBean中再封装到List集合中
   
   /**
     *  查询薪资大于 3000 的所员工信息,封装到JavaBean中再封装到List集合中
     *  BeanListHandler 将结果集的每一条和数据封装到JavaBean中 再将JavaBean 放到List集合中
     **/
    @Test
    public void testFindBySalary() throws SQLException {

        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from employee where salary > ?";

        List<Employee> query = qr.query(sql, new BeanListHandler<Employee>(Employee.class), 3000);

        for (Employee employee :query){
            System.out.println(employee);
        }
    }
   5).查询姓名是 张百万的员工信息,将结果封装到Map集合中
   
   /**
     * 查询姓名是 张百万的员工信息,将结果封装到Map集合中
     * MapHandler 将结果集的第一条记录封装到Map<String,Object> 中
     * key对应的是 列名 value对应的是 列的值
     */
    @Test
    public void testFindByName() throws SQLException {

        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select * from employee where ename = ?";

        Map<String, Object> map = qr.query(sql, new MapHandler(), "张百万");

        Set<Map.Entry<String, Object>> entries = map.entrySet();

        for (Map.Entry<String, Object> entry : entries){
            //打印结果
            System.out.println(entry.getKey() + " = " + entry.getValue());
        }
    }
   6).查询所有员工的薪资总额
   
   /**
     *  查询所有员工的薪资总额
     *  ScalarHandler 用于封装单个的数据
     */
    @Test
    public void testGetSum() throws SQLException {

        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "select sum(salary) from employee";

        Double sum = (Double) qr.query(sql, new ScalarHandler<>());

        System.out.println("员工薪资总额:" + sum);
    }
  
  
  