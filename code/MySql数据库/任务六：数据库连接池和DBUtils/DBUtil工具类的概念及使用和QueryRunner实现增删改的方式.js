 1.DBUtils概念
  
  使用JDBC我们发现冗余的代码太多了,为了简化开发我们选择使用DbUtils。
  Commons DbUtils是Apache组织提供的一个对JDBC进行简单封装的开源工具类库，使用它能够简化JDBC应用程序的开发，
同时也不会影响程序的性能。
  使用方式：
   DBUtils就是JDBC的简化开发工具包。需要项目导入commons-dbutils-1.6.jar。
 
 2.Dbutils核心功能介绍
   
   1). QueryRunner中提供对sql语句操作的API.
   2). ResultSetHandler接口，用于定义select操作后，怎样封装结果集。
   3). DbUtils类,他就是一个工具类,定义了关闭资源与事务处理相关方法。
 
 3.表和类之间的关系
 
   整个表可以看做是一个类
   表中的一行记录,对应一个类的实例(对象)
   表中的一列,对应类中的一个成员属性
   
 4.JavaBean组件
   1) JavaBean就是一个类,开发中通常用于封装数据,有一下特点
    需要实现序列化接口, Serializable (暂时可以省略)
	提供私有字段: private类型变量名
	提供getter和setter
	提供 空参构造
   2)创建Employee类和数据库的employee表对应
     我们可以创建一个entity包,专门用来存放JavaBean类
 
 5.QueryRunner核心类
 
   构造方法
     QueryRunner()
	 QueryRunner(DataSource ds),提供数据源（连接池），DBUtils底层自动维护连接connection
   常用方法
     update(Connection conn,String sql,Object...params)，用来完成表数据的增加、删除、更新操作
	 query(Connection conn,String sql,ResultSetHandler<T>，rsh,Object...params)，用来完成表数据的查询操作
 
 6.QueryRunner的创建
   
    手动模式
	  //方式1 手动模式
      QueryRunner qr = new QueryRunner();
	自动模式
	  //方式2 自动方式 提供数据库连接池对象 DBUtils会自动连接
      QueryRunner qr2 = new QueryRunner(DruidUtils.getDataSource());

 7.QueryRunner实现增、删、改操作
 
   核心方法 update(Connection conn, String sql, Object... params)
   参数                                说明
   Connection conn       数据库连接对象,自动模式创建QueryRun
                             可以不传,手动模式必须传递     
   String sql             占位符形式的SQL ,使用?号占位符
   Object... param   Object类型的可变参,用来设置占位符上的参数 
   步骤
    1).创建QueryRunner(手动或自动)
    2).占位符方式编写SQL
    3).设置占位符参数
    4).执行
    添加
    public class DBUtilsDemo02 {

    @Test
    public void testInsert() throws SQLException {

        //1.创建 QueryRunner 手动模式创建
        QueryRunner qr = new QueryRunner();

        //2.编写 占位符方式 SQL
        String sql = "insert into employee values(?,?,?,?,?,?)";

        //3.设置占位符的参数
        Object[] param = {null,"张百万",20,"女",10000,"1990-12-26"};

        //4.执行 update方法
        Connection con = DruidUtils.getConnection();
        qr.update(con,sql,param);

        //5.释放资源
        DbUtils.closeQuietly(con);

    }
}	
    修改
	//修改操作 修改姓名为 张百万的员工的工资为 15000
    @Test
    public void testUpdate() throws SQLException {
        //1.创建 核心类 自动模式 需要传递 数据库连接池对象
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        //2.编写SQL
        String sql = "update employee set salary = ? where ename =?";

        //3.设置占位符放入参数
        Object[] param ={15000,"张百万"};

        //4.执行修改操作 自动模式不需要传入Connection对象
        qr.update(sql,param);
    }
	删除
	//删除操作 删除id为1的 记录
    @Test
    public void testDelete() throws SQLException {
        QueryRunner qr = new QueryRunner(DruidUtils.getDataSource());

        String sql = "delete from employee where eid = ?";

        //如果只有一个参数的话 不需要创建数组
        qr.update(sql,1);
    }

   
