 1.C3P0连接池的概念
 
  C3P0是一个开源的JDBC连接池,支持JDBC3规范和JDBC2的标准扩展。目前使用它的开源项目有Hibernate、Spring等
 
 2.导入jar包及配置文件
 
  1)将jar包复制到myJar文件夹即可,IDEA会自动导入
  2)导入配置文件c3p0-config.xml
    c3p0-config.xml文件名不可更改
    直接放到src下,也可以放到到资源文件夹中
  3)在项目下创建一个resource文件夹(专门存放资源文件)
     在当前项目下创建一个resources文件夹
  4)选择文件夹,右键将resource文件夹指定为资源文件夹
    右键点击Mark Directory as 按钮下面的 Resources Root
  5)将文件放在resource目录下即可,创建连接池对象的时候会去加载这个配置文件
  
 3.编写C3P0工具类
   
   C3P0提供的核心工具类,ComboPooledDataSource,如果想使用连接池,就必须创建该类的对象
   new ComboPooledDataSource();使用默认配置
   new ComboPooledDataSource("mysql"); 使用命名配置
   public class C3P0Utils {

    //1.创建连接池对象 C3P0对DataSource接口的实现类
    //使用的配置是 配置文件中的默认配置
    //public static ComboPooledDataSource dataSource = new ComboPooledDataSource();

    // 使用指定的配置
    public static ComboPooledDataSource dataSource = new ComboPooledDataSource("mysql");

    //获取连接的方法
    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    //5.释放资源方法
    public static void close(Connection con, Statement statement){
        if(con != null && statement != null){
            try {
                statement.close();
                //归还连接
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Connection con, Statement statement, ResultSet resultSet){
        if(con != null && statement != null && resultSet !=null){
            try {
                resultSet.close();
                statement.close();
                //归还连接
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
  
 4.测试工具类
   
   需求:查询姓名为 李白的员工信息
   public class TestC3P0 {

    //需求:查询姓名为 李白的员工信息
    public static void main(String[] args) throws SQLException {
        
        //1.获取连接
        Connection con = C3P0Utils.getConnection();

        //2.获取预处理对象
        String sql = "select * from employee where ename = ?";
        PreparedStatement preparedStatement = con.prepareStatement(sql);

        preparedStatement.setString(1,"李白");
        ResultSet resultSet = preparedStatement.executeQuery();

        //4.处理结果集
        while (resultSet.next()){
            int eid = resultSet.getInt("eid");
            String ename = resultSet.getString("ename");
            int age = resultSet.getInt("age");
            String sex = resultSet.getString("sex");
            double salary = resultSet.getDouble("salary");
            Date date = resultSet.getDate("empdate");

            System.out.println(eid + ":" + ename + ":" +age + ":" + sex + ":" + salary + ":" + date);
        }

        //5.释放资源
        C3P0Utils.close(con,preparedStatement,resultSet);


    }

 5.常见配置
  
    分类：必须项(user -- 用户名 password -- 密码 driverClass -- 驱动 jdbcUrl -- 路径)
	基本配置：
	initialPoolSize -- 连接池初始化创建数 默认值：3
	maxPoolSize     -- 连接池中拥有的最大连接数 默认值：15
	minPoolSize     -- 连接池保持的最小连接数 默认值：10
	maxIdleTime     -- 连接的最大空闲时间 如果超过这个时间，某个数据库连接还没有被使用，则会断开掉
	                这个连接，如果为0，则永远不会断开连接 默认值：0
	
	
   
