 1.Druid连接池的概念
  
  Druid(德鲁伊)是阿里巴巴开发的号称为监控而生的数据库连接池，Druid是目前最好的数据库连接池。在功能、性能、扩展性方面
,都超过其他数据库连接池，同时加入了日志监控，可以很好的监控DB池连接和SQL的执行情况。

 2.导入jar包及配置文件
  
  1)导入jar包
  2)导入配置文件
    是properties形式的
    可以叫任意名称，可以放在任意目录下,我们统一放到resources资源目录
 
 3.编写Druid工具类
   
   获取数据库连接池对象
   通过工厂来来获取DruidDataSourceFactory类的createDataSource方法
   createDataSource(Properties p)方法参数可以是一个属性集对象
   public class DruidUtils {

    //1.定义成员变量
    public  static DataSource dataSource;

    //2.静态代码块
    static{
        try {
            //3.创建属性集对象
            Properties p = new Properties();

            //4.加载配置文件Druid 连接池不能够主动加载配置文件，需要指定文件
            InputStream inputStream = DruidUtils.class.getClassLoader().getResourceAsStream("druid.properties");

            //5.使用Properties对象的load方法 从字节流中读取配置信息
            p.load(inputStream);

            //6.通过工厂类获取连接池对象
            dataSource= DruidDataSourceFactory.createDataSource(p);
        }  catch (Exception e) {
            e.printStackTrace();
        }

    }

    // 获取连接的方法
    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
    //5.释放资源方法
    public static void close(Connection con, Statement statement){
        if(con != null && statement != null){
            try {
                statement.close();
                //归还连接
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(Connection con, Statement statement, ResultSet resultSet){
        if(con != null && statement != null && resultSet !=null){
            try {
                resultSet.close();
                statement.close();
                //归还连接
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
  
 4.测试工具类
   
   需求:查询薪资在3000 - 5000 元之间的员工姓名
   public class TestDruid {

    // 需求 查询 薪资在3000 到 5000之间的员工的姓名
    public static void main(String[] args) throws SQLException {

        //1.获取连接
        Connection con = DruidUtils.getConnection();

        //2.获取Statement对象
        Statement statement = con.createStatement();

        //3.执行查询
        ResultSet resultSet = statement.executeQuery("select ename from employee where salary between 3000 and 5000");

        //4.处理结果集
        while (resultSet.next()){
            String ename = resultSet.getString("ename");
            System.out.println(ename);
        }

        //5.释放资源
        DruidUtils.close(con,statement,resultSet);
    }
}
   