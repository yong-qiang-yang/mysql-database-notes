 1.DML操作表中数据
 
   SQL中的DML 用于对表中的数据进行增删改操作
 
 2.插入数据
   
   语法格式 insert into 表名(字段名1, 字段名2...) value(字段值1, 字段值2...);
   
   1)代码准备,创建一个学生表：
   表名：student
   表中字段：
       学员ID, sid int
	   姓名, sname varchar(20)
	   年龄, age int
	   性别, sex char(1)
	   地址, address varchar(40)
	   
   -- 创建学生表
   CREATE TABLE student(
	sid INT,
	sname VARCHAR(20),
	age INT,
	sex CHAR(1),
	address VARCHAR(40)
    );
    
   2)向学生表中添加数据，3种方式
     -- 方式1 插入全部字段 将所有字段名都写出来
    INSERT INTO student (sid,sname,age,sex,address) VALUES(1,'孙悟空',18,'男','花果山');
	
	-- 方式2 插入全部字段 不写字段名
    INSERT INTO student VALUES(2,'孙悟饭',5,'男','地球');
	
	-- 方式3 插入指定字段的值
    INSERT INTO student (sid,sname) VALUES(3,'蜘蛛精');
	
	注意:
	 1) 值与字段必须对应 个数相同&数据类型相同
	 2) 值的数据大小，必须在字段指定的长度范围内
	 3) varchar char date 类型的时候,必须要使用 单引号 或者双引号进行包裹
	 4) 如果插入空值 可以忽略不写 或者写 null
	 5) 如果插入指定字段的值，必须要上写列名
	 
	 -- 值得字段必须对应   1    2    3   4    5             1    2      3   4     5
	 INSERT INTO student (sid,sname,age,sex,address) VALUES(1,'孙悟空',18,'男','花果山');
	 -- 值的大小必须在字段指定的范围内               超出指定的长度范围|sex   char(1)
	 INSERT INTO student (sname,sex) VALUES('白骨精','woman')
	 -- 除了数值类型以外，其他类型都需要使用 双引号或单引号包裹
	 INSERT INTO student (sname,sex) VALUES(蜘蛛精,'女'); 蜘蛛精使用双引号或单引号包裹 才可以
	 -- 插入空值 可以忽略不写字段 或者插入空值
	 -- sid 插入空值
	 INSERT INTO student (sid,sname,age,sex,address) VALUES(NULL,'白龙马'30,'男','东海');
	 -- 忽略不写字段
	 INSERT INTO student (sname,age,sex,address) VALUES('蜘蛛精',18,'女','盘丝洞');
	 -- 如果插入指定字段的值，必须要上写列名  1 sid字段没有指定
	 INSERT INTO student (sname,age) VALUES(1,'如来',1000);
 
 3.更改数据
 
   语法格式1: 不带条件的修改 update 表名 set 列名 = 值
   语法格式2: 带条件的修改   update 表名 set 列名 = 值 [where 条件表达式: 字段名 = 值] 
   
   1)不带条件的修改，将所有的性别改为女(慎用!!)
   UPDATE student SET sex = '女';
   
   2)带条件的修改 将sid为 3的学生,性别改为男
   UPDATE student SET sex = '男' WHERE sid = 3;
   
   3)一次性修改多个列，将sid为2的学员，年龄改为20，地址改为北京
   UPDATE student SET age = 20,address = '北京' WHERE sid = 2;
   
 4.删除数据
 
   语法格式1: 删除所有数据      delete from 表名;
   语法格式2: 指定条件 删除数据 delete from 表名 [where 字段名 = 值];
   
    1)删除 sid为 6的数据
    DELETE FROM student WHERE sid = 1;
	
	2)删除所有数据
    DELETE FROM student;
	
	3)如果要删除表中的所有数据，有两种做法
	  --1.delete from 表; 不推荐, 有多少条记录 就执行多少次删除操作，效率低
	  --2.truncate table 表名: 推荐, 先删除整张表, 然后再重新创建一张一模一样的表，效率高.
	  
	   TRUNCATE TABLE student;

    
	
	
	                                                