 1.DDL操作数据表
   
   MySQL常见的数据类型
           类型                              描述
           int                               整型
          double                            浮点型
          varchar                          字符串型
           date            日期类型，给是为yyyy-MM-dd ,只有年月日，没有时分秒
 
 注意：MySQL中的char类型与varchar类型，都对应了Java中的字符串类型，区别在于：
       char类型是固定长度的：根据定义的字符串长度分配足够的空间。
       varchar类型是可变长度的：只使用字符串长度所需的空间

 适用场景：char类型适合存储固定长度的字符串，比如密码，性别一类
           varchar类型适合存储在一定范围内，有长度变化的字符串

 2.创建表的语法格式

	create table 表名(
	   字段名称1 字段类型(长度),
	   字段名称2 字段类型,
	   字段名称3 字段类型 最后一个列不要添加逗号
	);
	
  需求1: 创建商品分类表
  /*
  表名 category
  表中字段:
	  分类id: cid, 为整型
	  分类名称:cname，为字符串类型，指定长度20
  */
  SQL实现
   USE dbi; -- 切换数据库 db1
   
   -- 创建分类表
CREATE TABLE category(
	cid INT,
	cname VARCHAR(20)
);

 需求2:创建测试表
 
  /*
	表名 test1
	表中字段:
		测试ID: tid，为整型 
		测试时间: tdate，为年月日的日期类型
  */
  SQL实现
  -- 创建测试表
  CREATE TABLE test1(
	tid INT,
	tdate DATE
);

 需求3: 快速创建一个表结构相同的表（复制表结构）
 语法格式： create table 新表名称 like 旧表名称
 代码示例
   -- 创建一个与test1表结构相同的 test2表
   CREATE TABLE test2 LIKE test1;

   -- 查看表结构
   DESC test1;
 
 3.查看表
   
   show tables;  -- 查看当前数据库中所有表名
   desc 表名;    -- 查看数据表的结构
   代码示例
   
   
   SHOW TABLES; -- 查看当前数据库中所有表名
   DESC category; -- 显示当前数据表的结构
   
 4.删除表
 
   drop table 表名;           -- 删除表(从数据库中永久删除某一张表)
   drop table if exists 表名; -- 判断表是否存在, 存在的话就删除,不存在就不执行删除
   代码示例：
   
   DROP TABLE test1;             -- 直接删除 test1 表
   DROP TABLE IF EXISTS test2;   -- 先判断 再删除 tset2表

 5.修改表
 
    1)修改表名称 语法格式: rename table 旧表名 to 新表名;
   
     需求: 将category表 改为 category1；
   
     RENAME TABLE category TO category1;
   
    2)修改表的字符集 语法格式 alter table 表名 character set 字符集;
   
     需求: 将category表的字符集 修改为gbk
	 
	 alter table category character set gbk;
   
    3)向表中添加列，关键字 ADD 语法格式 alter table 表名 add 字段名称 字段类型;
	
	 需求: 为分类表添加一个新的字段为 分类描述 cdesc varchar(20)

       -- 为分类表添加一个新的字段为 分类描述 cdesc varchar(20)
       ALTER TABLE category1 ADD cdesc VARCHAR(20);	 

    4)修改表中列的 数据类型或长度，关键字 MODIFY 语法格式 alter table 表名 modify 字段名称 字段类型;

      需求: 对分类表的描述字段进行修改，类型varchar(50)
      
      ALTER TABLE category MODIFY cdesc VARCHAR(50);

    5)修改列名称 关键字 CHANGE 语法格式 alter table 表名 change 旧列名 新列名 类型(长度);

     需求: 对分类表中的desc字段进行更换，更换为description varchar(30)

      ALTER TABLE category CHANGE cdesc description VARCHAR(30);
	
	6)删除列 关键字 DROP 语法格式 alter table 表名 drop 列名;
	
	  需求: 删除分类表中description这列
	  
	   ALTER TABLE category DROP description;
	

	 
   
 
   
   
  
  
  
  
  