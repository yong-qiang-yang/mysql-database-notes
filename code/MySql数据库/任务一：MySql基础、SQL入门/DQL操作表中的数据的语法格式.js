 1.DQL查询表中数据
   准备数据
   -- 创建员工表
   表名 emp
   表中字段:
       eid 员工id, int
	   ename 姓名, varchar
	   sex 性别, char
	   salary 薪资, double
	   hire_date 入职时间, date
	   dept_name 部门名称, varchar
	
   -- 创建员工表
   CREATE TABLE emp(
       eid INT
       ename VARCHAR(20),
       sex CHAR(1),
       salary DOUBLE,
	   hire_date DATE,
	   dept_name VARCHAR(20)
   );
   
   -- 添加数据
   INSERT INTO emp VALUES(1,'孙悟空','男',7200,'2013-02-04','教学部');
   INSERT INTO emp VALUES(2,'猪八戒','男',3600,'2010-12-02','教学部');
   INSERT INTO emp VALUES(3,'唐僧','男',9000,'2008-08-08','教学部');
   INSERT INTO emp VALUES(4,'白骨精','女',5000,'2015-10-07','市场部');
   INSERT INTO emp VALUES(5,'蜘蛛精','女',5000,'2011-03-14','市场部');
   INSERT INTO emp VALUES(6,'玉兔精','女',200,'2000-03-14','市场部');
   INSERT INTO emp VALUES(7,'林黛玉','女',10000,'2019-10-07','财务部');
   INSERT INTO emp VALUES(8,'黄蓉','女',3500,'2011-09-14','财务部');
   INSERT INTO emp VALUES(9,'吴承恩','男',20000,'2000-03-14',NULL);
   INSERT INTO emp VALUES(10,'孙悟饭','男',10,'2020-03-14','财务部');
   INSERT INTO emp VALUES(11,'兔八哥','女',300,'2010-03-14','财务部');
 
 2.简单查询
 
   查询不会对数据库中的数据进行修改.只是一种显示数据的方式SELECT。
   语法格式: select 列名 from 表名
   
   需求1: 查询emp 表中的所有数据
    SELECT * FROM emp; -- 使用 * 表示所有的列
	
   需求2: 查询emp表中的所有记录, 只显示 id 和 name字段
     SELECT eid, ename FROM emp;
	 
   需求3: 将所有的员工信息查询出来,并将列名 改为中文
   
      -- 别名查询 使用关键字 as
	  --使用 AS关键字，为列起别名
	SELECT 
	eid AS '编号',
	ename AS '姓名',
	sex AS '性别',
	salary AS '薪资',
	hire_date AS '入职时间',
	dept_name '部门名称'  -- as 可以省略

    FROM emp;
	
	需求4: 查询一共有几个部门
	使用去重关键字 distinct
	
	-- 使用distinct 关键字，去掉重复部分信息 
     SELECT DISTINCT dept_name FROM emp;
	 
	需求5: 将所有员工的工资 +1000 元进行显示
	运算查询(查询结果参与运算)
	SELECT ename, salary+1000 AS salary FROM emp; -- 将我们的员工薪资数据 +1000 进行展示
	
 3.条件查询
 
   如果查询语句中没有设置条件,就会查询所有的行信息,在实际应用中,一定要指定查询条件,对记录进行过滤。
   语法格式：select 列名 from 表名 where 条件表达式
    先取出表中的每条数据，满足条件的数据就返回，不满足的过滤掉
	
 4.运算符
     1)比较运算符
	       运算符                                           说明
      >  <  <=   >=   =  <> !=                大于、小于、大于(小于)等于、不等于
	  BETWEEN  ...AND...            显示在某一区间的值(2000-10000之间：Between 2000 and 10000)
	  IN(集合)                       集合表示多个值,使用逗号分隔,例如:name in (悟空，八戒)
	                                  in中的每个数据都会作为一次条件,只要满足条件就会显示
	  LIKE                                               模糊查询
	  IS NULL                                查询某一列为NULL的值,注:不能写= NULL
	  
	2)逻辑运算符
	      运算符                                          说明
	      And &&                                    多个条件同时成立
		  Or ||                                     多个条件任一成立
		   Not                                        不成立，取反
	
 5.需求1:
 
    # 查询员工姓名为黄蓉的员工信息
    # 查询薪水价格为5000的员工信息
    # 查询薪水价格不是5000的所有员工信息
	# 查询薪水价格大于6000元的所有员工信息
    # 查询薪水价格在5000到10000之间所有员工信息
    # 查询薪水价格是3600或7200或者20000的所有员工信息
	
   代码实现
   
    SELECT * FROM emp WHERE ename = '黄蓉'; -- 查询员工姓名为黄蓉的员工信息
	
	SELECT * FROM emp WHERE salary = 5000;  -- 查询薪水价格为5000的员工信息
	
	SELECT * FROM emp WHERE salary != 5000; -- 查询薪水价格不是5000的所有员工信息
	SELECT * FROM emp WHERE salary <> 5000; -- 查询薪水价格不是5000的所有员工信息
	
	SELECT * FROM emp WHERE salary > 6000;  -- 查询薪水价格大于6000元的所有员工信息
	
	SELECT * FROM emp WHERE salary BETWEEN 5000 AND 10000; -- 查询薪水价格在5000到10000之间所有员工信息
	
	-- 查询薪水价格是3600或7200或者20000的所有员工信息
	-- 方式1: or
	SELECT * FROM emp WHERE salary = 3600 or salary = 7200 or salary = 20000;
	-- 方式2: in() 匹配括号中指定的参数
	SELECT * FROM emp WHERE salary in(3600,7200,20000);
	
 6.模糊查询 通配符
     
	 %: 表示匹配任意多个字符串;
	 _: 表示匹配一个字符
 
 7.需求2:
 
   # 查询含有'精'字的所有员工信息
   # 查询以'孙'开头的所有员工信息
   # 查询第二个字为'兔'的所有员工信息
   # 查询没有部门的员工信息
   # 查询有部门的员工信息
   
     代码实现
	 
	 SELECT * FROM emp WHERE ename LIKE '%精%';     -- 查询含有'精'字的所有员工信息
	 
	 SELECT * FROM emp WHERE ename LIKE '孙%';      -- 查询以'孙'开头的所有员工信息
	 
	 SELECT * FROM emp WHERE ename LIKE '_兔%';     -- 查询第二个字为'兔'的所有员工信息
	 
	 SELECT * FROM emp WHERE dept_name IS NULL;     -- 查询没有部门的员工信息
	 -- SELECT * FROM emp WHERE dept_name = NULL; 这种写法不正确
	 
	 SELECT * FROM emp WHERE dept_name IS NOT NULL; -- 查询有部门的员工信息
	 
	 
    
 