 1.SQL的概念
 
   结构化查询语言(Structured Query Language)简称SQL，是一种特殊目的的编程语言，是一种数据库查询和程序设计语言，用于
存取数据以及查询、更新和管理关系数据库系统。

 2.SQL的作用
 
   是所有关系型数据库的统一查询规范，不同的关系型数据库都支持SQL
   所有的关系型数据库都可以使用SQL
   不同数据库之间的SQL有一些区别 方言
 
 3.SQL通用语法
 
   SQL语句可以单行或者多行书写，以分号结尾;(Sqlyog中可以不用写分号)
   可以使用空格和缩进来增加语句的可读性。 
   MySql中使用SQL不区分大小写，一般关键字大写，数据库名 表名 列名 小写
   注释方式：
         注释语法                   说明
	      --空格                  单行注释
		  /* */                   多行注释
		    #                 MySql特有的单行注释
	# show databases;   单行注释
     -- show databases; 单行注释
     /*
     多行注释
     show databases;
     */
 4.SQL的分类
 
  数据定义语言:简称DDL(Data Definition Language)，用来定义数据库对象：数据库，表，列等。
  数据操作语言:简称DML(Data Manipulation Language)，用来对数据库中表的记录进行更新。
  数据查询语言:简称DQL(Data Query Language)，用来查询数据库中表的记录。
  数据控制语言:简称DCL(Date Control Language)，用来定义数据库的访问权限和安全级别,及创建用户(了解)
  注：我们重点学习DML与DQL

 5.DDL操作 (创建数据库)
  
   create database 数据库名; --创建指定名称的数据库
   create database 数据库名 character Set 字符集; -- 创建指定名称的数据库，并且指定字符集(一般都指定utf-8)
   
    代码示例：
	 /*
	创建数据库 方式1: 指定名称的数据库
	latin1 编码
    */
    CREATE DATABASE db1;

    /*
	指定字符集的方式创建数据库
	utf8
     */
    CREATE DATABASE db1_1 CHARACTER SET utf8;
 
 6.查看/选择数据库
   
    use 数据库 -- 切换数据库
	select database; -- 查看当前正在使用的数据库
	show databases; -- 查看Mysql中都有哪些数据库
	show create database 数据库名; -- 查看一个数据库的定义信息

	代码示例：
	  /*
	查看数据库
     */
     
     USE db1_1;    -- 切换数据库 
     
	 SELECT DATABASE(); -- 查询当前正在使用的数据库    
     
	 SHOW DATABASES; -- 查询MySql中有哪些数据库
	 
	 SHOW CREATE DATABASE db1_1; -- 查看一个数据库的定义信息
	 
 7.修改数据库
    
	alter database 数据库名 character set utf8; -- 数据库的字符集修改操作
	
	alter database db1 charcter set utf-8; -- 将数据库db1的字符集修改为utf-8
	
	show create database bd1; -- 查看当前数据库的基本信息
 
 8.删除数据库
 
   drop database 数据库名称 --将数据库从MySql中永久删除

   drop database db1_1;
   
   
  
  
